<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://gitlab.com/slrondonm/geonamesorg
 * @since      1.0.0
 *
 * @package    Geonamesorg
 * @subpackage Geonamesorg/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Geonamesorg
 * @subpackage Geonamesorg/includes
 * @author     Sergio Lankaster Rondón Melo <sl.rondon.m@gmail.com>
 */
class Geonamesorg_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
