<?php

/**
 * Fired during plugin activation
 *
 * @link       https://gitlab.com/slrondonm/geonamesorg
 * @since      1.0.0
 *
 * @package    Geonamesorg
 * @subpackage Geonamesorg/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Geonamesorg
 * @subpackage Geonamesorg/includes
 * @author     Sergio Lankaster Rondón Melo <sl.rondon.m@gmail.com>
 */
class Geonamesorg_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
